import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbButtonModule, NbInputModule, NbCardModule, NbIconModule, NbSelectModule, NbDialogModule, NbListModule, NbActionsModule } from '@nebular/theme';
import { LoaderComponent } from './components/loader/loader.component';



@NgModule({
  declarations: [LoaderComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NbButtonModule,
    NbInputModule,
    NbCardModule,
    NbIconModule,
    NbSelectModule,
    NbDialogModule,
    NbListModule,
    NbActionsModule,
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    NbButtonModule,
    NbInputModule,
    NbCardModule,
    NbIconModule,
    NbSelectModule,
    NbDialogModule,
    NbListModule,
    NbActionsModule,
  ],
  entryComponents: [LoaderComponent]
})
export class SharedModule { }
