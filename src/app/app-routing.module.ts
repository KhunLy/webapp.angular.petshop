import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = 
[
  { path: '', redirectTo: '/default/home', pathMatch: 'full' },
  { 
    path: 'auth', 
    loadChildren: () => import('./features/auth/auth.module')
  .then(m => m.AuthModule) },
  { path: 'pet', loadChildren: () => import('./features/pet/pet.module').then(m => m.PetModule) },
  { path: 'default', loadChildren: () => import('./features/default/default.module').then(m => m.DefaultModule) },
{ path: 'animal', loadChildren: () => import('./features/animal/animal.module').then(m => m.AnimalModule) }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
