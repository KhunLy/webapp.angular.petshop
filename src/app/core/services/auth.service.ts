import { Injectable } from '@angular/core';
import { LoginModel } from '../models/login.model';
import { RegisterModel } from '../models/resister.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private httpClient: HttpClient
  ) { }

  login(model: LoginModel):Observable<string> {
    return this.httpClient.post<string>(
      environment.apiDomain + 'api/security/login',
      model
    );
  }

  register(model: RegisterModel) {
    return this.httpClient.post<string>(
      environment.apiDomain + 'api/user',
      model
    );
  }

  logout() {}
}
