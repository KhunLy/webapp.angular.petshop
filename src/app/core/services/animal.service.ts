import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { AnimalModel } from '../models/animal.model';
import { environment } from 'src/environments/environment';
import { finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AnimalService {

  private _context$: BehaviorSubject<AnimalModel[]>;

  public get context$(): Observable<AnimalModel[]> {
    return this._context$.asObservable();
  }

  constructor(
    private httpClient: HttpClient
  ) {
    this._context$ = new BehaviorSubject<AnimalModel[]>([]);
  }

  refresh():void {
    this.httpClient
      .get<AnimalModel[]>(environment.apiDomain + 'api/animal')
      .subscribe(data => this._context$.next(data));
  }

  delete(a: AnimalModel) : Observable<void> {
    return this.httpClient
      .delete<void>(environment.apiDomain + 'api/animal/' + a.name
    ).pipe(finalize(() => this.refresh()));
  }

  insert(a: AnimalModel): Observable<AnimalModel> {
    return this.httpClient
      .post<AnimalModel>(environment.apiDomain + 'api/animal', a)
      .pipe(finalize(() => this.refresh()));
  }
}