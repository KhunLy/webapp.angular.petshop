import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { NbDialogService } from '@nebular/theme';
import { LoaderComponent } from 'src/app/shared/components/loader/loader.component';
import { finalize } from 'rxjs/operators';

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {

  constructor(
    private nbDialog: NbDialogService
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    //afficher le loader
    let ref = this.nbDialog.open(LoaderComponent, {
      closeOnBackdropClick: false
    });
    return next.handle(request).pipe(
      finalize(() => {
        ref.close()
      })
    );
    //fermer le loader
  }
}
