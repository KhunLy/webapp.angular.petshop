import { Component, OnInit } from '@angular/core';
import { NbMenuItem, NbSidebarService } from '@nebular/theme';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  
  items: NbMenuItem[];
  get isConnected(): boolean {
    return localStorage.getItem('TOKEN') != null
  }
  constructor(
    private nbSidebar: NbSidebarService,
    private router: Router
  ) {}
  ngOnInit() {
    this.items = [
      { title: 'Home', link: '/default/home', icon: 'home' },
      { title: 'Animals', link: '/animal', icon: 'github' },
    ]
  }

  collapse () {
    this.nbSidebar.toggle(true);
  }

  logout() {
    localStorage.clear();
    this.router.navigateByUrl('/auth/login');
  }
}
