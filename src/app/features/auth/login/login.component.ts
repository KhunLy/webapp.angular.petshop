import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup
  constructor(
    private authService: AuthService,
    private toastrService: NbToastrService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      'email': new FormControl(
        null,
        Validators.compose([
          Validators.required,
          Validators.email,
          Validators.max(255)
        ]) 
      ),
      'password': new FormControl(
        null,
        Validators.compose([
          Validators.required,
          Validators.max(255)
        ])
      )
    });
  }

  login() {
    this.authService.login(this.loginForm.value)
      .subscribe(token => {
        localStorage.setItem('TOKEN', token);
        this.toastrService.info('Bienvenue sur notre site !!')
        //message de success
        //rediriger le user
        this.router.navigateByUrl('/default/home');
      }, error => {
        console.log(error);
        this.toastrService.danger('Login ou mdp incorrect!!')
        //message d'erreur
      });
  }

}
