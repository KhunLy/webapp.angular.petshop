import { Component, OnInit } from '@angular/core';
import { AnimalModel } from 'src/app/core/models/animal.model';
import { AnimalService } from 'src/app/core/services/animal.service';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'app-animal',
  templateUrl: './animal.component.html',
  styleUrls: ['./animal.component.scss']
})
export class AnimalComponent implements OnInit {

  model: AnimalModel[];
  constructor(
    private animalService: AnimalService,
    private toastr: NbToastrService,
  ) { }

  ngOnInit(): void {
    this.animalService.refresh()
    this.animalService.context$
      .subscribe(data => this.model = data);
  }

  delete(toDelete : AnimalModel){
    this.animalService.delete(toDelete)
      .subscribe(() => {
        this.toastr.success('L\'animal a été supprimé');
        //this.animalService.context$.next([])
        // let index = this.model.indexOf(toDelete);
        // this.model.splice(index, 1);
      }, error => {
        console.log(error);
        this.toastr.danger('Une erreur est survenue');
      });
  } 

  add(a: AnimalModel) {
    this.model.push(a);
  }

}
