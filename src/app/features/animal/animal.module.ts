import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AnimalRoutingModule } from './animal-routing.module';
import { AnimalComponent } from './animal.component';
import { AnimalAddComponent } from './animal-add/animal-add.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [AnimalComponent, AnimalAddComponent],
  imports: [
    CommonModule,
    AnimalRoutingModule,
    SharedModule
  ]
})
export class AnimalModule { }
