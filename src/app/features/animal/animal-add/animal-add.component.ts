import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AnimalService } from 'src/app/core/services/animal.service';
import { NbToastrService } from '@nebular/theme';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AnimalModel } from 'src/app/core/models/animal.model';

@Component({
  selector: 'app-animal-add',
  templateUrl: './animal-add.component.html',
  styleUrls: ['./animal-add.component.scss']
})
export class AnimalAddComponent implements OnInit {

  //@Output() onAdd: EventEmitter<AnimalModel>;

  animalForm: FormGroup

  constructor(
    private animalService: AnimalService,
    private toastr: NbToastrService,
  ) { 
    //this.onAdd = new EventEmitter<AnimalModel>();
  }

  ngOnInit(): void {
    this.animalForm = new FormGroup({
      'name': new FormControl(null, Validators.compose([
        Validators.required,
        Validators.max(50)
      ]))
    })
  }

  create() {
    this.animalService.insert(this.animalForm.value)
      .subscribe(data => {
        this.toastr.success('Tout est Ok');
        //this.onAdd.emit(this.animalForm.value);
      }, error => {
        this.toastr.danger('Il y eu un problème');
      })
  }

}
